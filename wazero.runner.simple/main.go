package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/tetratelabs/wazero"
	"github.com/tetratelabs/wazero/imports/wasi_snapshot_preview1"
)

func main() {
	// Choose the context to use for function calls.
	ctx := context.Background()

	// Create a new WebAssembly Runtime.
	//wasmRuntime := wazero.NewRuntime(ctx)
	wasmRuntime := wazero.NewRuntimeWithConfig(ctx, wazero.NewRuntimeConfigInterpreter())

	defer wasmRuntime.Close(ctx) // This closes everything this Runtime created.

	/*
		Package wasi_snapshot_preview1 contains Go-defined functions to access system calls, 
		such as opening a file, similar to Go's x/sys package. 
		These are accessible from WebAssembly-defined functions via importing ModuleName. 
		All WASI functions return a single Errno result: ErrnoSuccess on success.
	*/
	wasi_snapshot_preview1.MustInstantiate(ctx, wasmRuntime)


	// Load the WebAssembly module
	wasmFilePath := "../function.add/target/wasm32-wasi/debug/add.wasm"
	addWasmModule, err := os.ReadFile(wasmFilePath)
	if err != nil {
		log.Panicln(err)
	}

	// Instantiate the WebAssembly module
	mod, err := wasmRuntime.InstantiateModuleFromBinary(ctx, addWasmModule)
	if err != nil {
		log.Panicln(err)
	}

	// Get references to WebAssembly function: "add"
	addWasmFunction := mod.ExportedFunction("add")
	
	// Now, we can call "add", which reads the data we wrote to memory!
	// result []uint64
	result, err := addWasmFunction.Call(ctx, 20, 22)
	if err != nil {
		log.Panicln(err)
	}

	fmt.Println("result:", result[0])

}
